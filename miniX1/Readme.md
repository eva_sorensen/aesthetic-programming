## This is my first MiniX1!

Link: https://eva_sorensen.gitlab.io/aesthetic-programming/miniX1/

Link to the repository (the code): https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX1/sketch.js

Here are some screenshots.. You can go through the **link** to see and try the program yourself!

![](skærmbillede_minix1_01.png)
![](skærmbillede_minix1_02.png)

----

#### About my MiniX1

As my first RunMe i've created some circles that change color and size depending on the placement of the mouse on the x-axis of the canvas. If the mouse is moved to the left side of the canvas, a text will appear that tell you to right - when you do so, and get to the right side of the canvas a text will appear that says "This is fun :)" - i didn't really have any specific to tell, but i knew i wanted to include some text. I did this by using if-statement so that the text doesn't show all the time. The reference page on p5.js.org was very helpful doing this. 

I got inspiration from examples on the p5.js-website. Mostly i used `map()`. This made it possible for me to change the size and color of the ellipses, which was my main focus. When they get bigger, their color gets lighter. I really like that you can interact with the program, and it was pleasing when it finally worked!

I had some different challenges during the process. In the beginning i put the background(0) under setup, which i actually didn't think would make a difference. But when i ran the program, the ellipses stayed where they had went, when the mouse where as far to the right as it could be. In other words, they didn't respond, when i moved the mouse to the left again. This also ment that the text got concealed behind the ellipses. I wanted to fix this. After moving all of the different sections of code around, i figured out that i only had to move background(0) from the setup to the function draw.. I guess this shows that it does matter where you put the different code and you need to think about this. 
In addition i also realised that the sequence the objects are put in, determines which is "on top" in the program. This was fine, put it led me to making the ellipses a bit transparent, so that they in some way all was visible also when they get bigger. This makes sense because it's the same when you read the code: you keep adding on top of the first thing written. 

In general, i really liked coding for the first time! It's an interesting process - you start with an idea, but have no idea if you can do it or not. Througout the process it can be really frustrating when it doesn't work as you thought it would, but when it finally does it's so worth the time! I'm looking forward to learning more about programming and hopefully i will produce some more interesting RunMe's. 

**References**

- https://p5js.org/examples/math-map.html
- https://p5js.org/reference/#/p5/if-else


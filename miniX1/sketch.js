function setup() {
  createCanvas(600, 500);
  noStroke();

}

function draw() {
background(0);
let d = map(mouseX, 0, width, 40, 300);
let c = map(mouseX, 0, width, 70, 175);

fill(69, c, 182, 150);
ellipse(200, 100, d, d);

fill(150, c, 182, 160);
ellipse(430, 330, d, d);

fill(c, 65, 138, 140);
ellipse(150, 400, d, d);

fill(c, 100, 150, 130);
ellipse(560, 80, d, d);

fill(c, 70, 150, 180);
ellipse(360, 200, d, d);

fill(100, c, 182, 220);
ellipse(70, 230, d, d);

if(d > 260) {
  textSize(55)
  fill(61, 231, 135);
  text('THIS IS FUN :)', 120, 250);
}
if(d < 50) {
  textSize(40);
  fill(61, 231, 135);
  text('GO RIGHT', 200, 250);
}
}

### MINIX2

----

This is my miniX2, [click here](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX2/) to show. 
And [click here](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX2/sketch.js) to show my repository (code). 

This week we had to focus on emojis; think about and reflect upon them - their political and aesthetic influence. My thoughts upon this subject were based on my subjective experiences with emojis and the use of them in online conversations. They've become quite influencial in many peoples texting and expression of emotions when we communicate over the internet, no matter the platform. 

My RunMe consists of two emojis. The one to the right is a simple smiling face, and the one to the left is grumpy. When the user clicks the mouse some elements are added to the canvas. These make the "emojis" change their expression - now the left one seems scared/surprised and the right one is smiling. The basics of the emojis did not change, some things were just added. Below you see screenshots of the two different stages in my RunMe. Notice how the fundamentals of the emoji's are still the same, even though the expression has totally changed. 

![](skærmbillede_minix2_01.png)
![](skærmbillede_minix2_02.png)

I specifically chose to keep the colors just simple black and white not to include any more difficulies and confusement; just focusing on the shapes of the faces. 

Through the process of making this code i had a few difficulties. In the beginning i had no idea what i was going to make - so i just started making these two faces - one happy and one sad. That's where i got the idea to change them, just by layering new objects on top, exactly to point out the earlier described issues. 
I had some help from the instructors on the layering of the code, because having both an ellipse following the curser(mouseX, mouseY) and making some extra objects appear when clicking the mouse made some errors when running the program. When one of these thing would work, the other did not. The solution was to put the background and ellipse (the ellipse following the mouse) under draw and adding clear(), so that ellipses that otherwise would have kept showing on the canvas got erased.  

To make the new objects appear when clicking the mouse, i used an if-statement in addition to true/false. When the mouse is clicked, the switch1 becomes true, which leads the if-statement to run. In the beginning i didn't know how to swtich back, but i got help and made it work. 

In my experience, emojis can both improve our understanding of eachother, but on the other hand they can also lead to misunderstandings when used differently by different people and in improper situations. That is why i've chose to make two emojis in my RunMe that totally changes expression when the mouse is clicked. I did this to emphasize this attribute of emojis, and just how something as easy and simple (like emojojis) can become something else and misunderstood. 
In general, i think that the internet is a place where many people let out emotions (both good and bad), but a lot of people doesn't really realise what they write actually have an impact on other people, when they don't see this themselves - but it does. This RunMe is supposed to show the user just how quickly and easily one can misunderstand intensions, when writing online. 
I know emojis doesn't change shapes and likewise when we use them, but the point of this is to show how easily expressions change and maybe make the user think about whether the recipient of the emoji will percieve it the same way you, as the sender, intented. 

I hope this RunMe and ReadMe make the reader think about the difference in communicating with people in real life vs. online.

**References**

- https://p5js.org/reference/#/p5/clear


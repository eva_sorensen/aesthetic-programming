let c = 30;
let e = 260;

let switch1 = false;

function setup() {
  createCanvas(windowWidth, windowHeight);

}

function draw() {

  clear();
  background(0);
  //curser
    noFill();
    stroke(200);
    strokeWeight(2);
    ellipse(mouseX, mouseY, 40, 40);

  textSize(22);
  fill(255, 148, 0);
  noStroke();
  text('Hello! Click the mouse to find out just how easy the look of emojis and their expression change', 30, 30);

  noFill();
  stroke(255);
  strokeWeight(4);
  //mouths
  arc(300, 350, 100, 80, 0, PI, OPEN);
  arc(700, 375, 100, 80, PI, TWO_PI);

  //eyes left
  circle(250, 250, 50);
  circle(350, 250, 50);
  //eyes right
  circle(650, 250, 50);
  circle(750, 250, 50);


  if (switch1) {
    fill(255);
    noStroke();
    //eyes lesft
    ellipse(250, 240, c, c);
    ellipse(350, 240, c, c);

    ellipse(650, e, c, c);
    ellipse(750, e, c, c);

    noFill();
    stroke(255);
    strokeWeight(4);

    //open O mouth
    arc(300, 350, 100, 80, PI, TWO_PI);

    //extra mouth right
    arc(700, 335, 160, 110, 0, PI, CHORD);

  }
}

function mouseClicked () {

  if(switch1 == false){
      switch1 = true;
  }else {
    switch1 = false;
  }


  }

let cells = [];
let drops = [];
let cellSize = 15;
let numRows;
let numCols;
let dropTimeout = 1;
let maxOffscreen = 200;
let brightTime = 60;

function setup() {
  createCanvas(windowWidth, windowHeight);
  textStyle(BOLD);
  textSize(18);

  frameRate(100);

  numRows = ceil(height / cellSize);
  numCols = ceil(width / cellSize);

  // make the cells according to rows
  for (let i = 0; i < numRows; i++){
    let newRow = [];
    for (let j = 0; j < numCols; j++) {
      newRow.push(new Cell(j * cellSize, i * cellSize));
    }
    cells.push(newRow);
  }

  for (let j = 0; j < numCols; j++) {
    drops.push(new Drop(j));
  }
}

function draw() {
  background(0);

  // conditions for each drop
  drops.forEach(drop => {
    drop.update();
    drop.brightenCell();
  });

  cells.forEach(row => {
    row.forEach(cell => {
      cell.draw();
      cell.update();

    });
  });
}

// declaration of cells
class Cell {
  constructor(x, y){
    this.x = x;
    this.y = y;
    this.brightness = 0;
    this.litTimer = 0;
    this.symbol = this.getRandomSymbol();
  }

  brighten() {
    this.brightness = 80;
  }

  getRandomSymbol(){
    return random("AABBCDEEFGHIIJKKLMNOOPQRRSSXTUZ".split("")); // "random" letter
    /* I put some letters two times, to increase the possibility
    of these being picked. Also chose not to put ÆØÅ*/
  }

  update() {
    if (this.brightness > 0) {
      this.brightness = 80;
      this.litTimer =  (this.litTimer + 1) % brightTime;

      if (this.litTimer === 0) {
        this.brightness = 0;
      }
    }
  }

  draw() {
    //fill(this.brightness, 0, 0); // red
    //fill(0, this.brightness, 0); // green
    //fill(0, 0, this.brightness); // blue
    fill(this.brightness, 0, this.brightness); // pink
    //text('∣', this.x, this.y);
    text(this.symbol, this.x, this.y);
  }
}

// class: how the cells drop
class Drop {
  constructor(col) {
    this.row = 0;
    this.col = col;
    this.dropTimeout = 0;
    this.offScreenTimeout = floor(random(maxOffscreen));
    this.offScreen = true;
  }

  update() {
    if (this.offScreen) {
      this.offScreenTimeout = (this.offScreenTimeout + 1) % maxOffscreen;

      if (this.offScreenTimeout === 0){
        this.offScreen = false;
      }
    } else {
      this.dropTimeout = (this.dropTimeout + 1) % dropTimeout;

      if (this.dropTimeout === 0){
        this.row++;
      }

      if (this.row === numRows){
        this.row = 0;
        this.offScreen = true;
        this.offScreenTimeout = floor(random(maxOffscreen));
      }
    }
  }

  brightenCell() {
    if (!this.offScreen)
     cells[this.row][this.col].brighten();
  }
}

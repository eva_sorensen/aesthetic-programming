## MiniX5 - "A Generative Program"


[My program](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX5/)

[My repository](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX5/sketch.js)

A screenshot of the program:

![](ord.png)

____

I've chosen to call it "Raining Words". When you run the program, you can try to find any emerging words for yourself. Otherwise, i've highlighted some in the screenshot for you to see.. both in english and danish and just anything i just thought made sense. 

The program is inspired by the intro of "The Matrix", you can see it here if you'd like (from 0:30): https://www.youtube.com/watch?v=KoQOKq1C3O4. You can also see where i got most of my code from and other inspirations in references. 

**– What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**

My program consists of several for-loops and conditional statements to make the lines of letters "drop" from the top of the canvas. When the program starts, it just keeps running and producing new random lenghts of the lines and appearing in random places of the width of the canvas. So in that way, the program keeps producing new cases of different emerging behaviors in the way that the letters by coincidence can make a word if placed in a correct but random spot. 

**– What role do rules and processes have in your work?**

They determine all of the "movement" of the program, which means that the program just needs to be started and then it will continue running by itself. 

**– Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

When i thought about this program, i mostly thought about the different kind of randomness (or indeterminacy) described in the 10 PRINT. Mostly, i think this program represents formal indeterminacy (often known as chance). It's because the randomness of this program makes it possible for a word to appear by chance (or just some shortening of a word maybe). I think of this to be the emergent behavior of this program. There also seems to be a general sceptism towards generative systems by computers because next step in a computer always is based on the previous, which (according to the text) makes every machine pseudorandom: 
> "… Computer-based random number generators are more technically described as pseudorandom number generators" (p. 130). 

So this is definitely something to think about when computer generating systems is used in different kinds of problemsolving. 

I have little control over the emergent behavior in the program, even though i made the rules of the program, but this is a good example of the role of auto-generating system in computers. As we discussed in class this week, the computer do not make the rules; it just executes the code, but how it will auto-generate something we will probably never quite understand or be able to foresee. Maybe that's what makes auto-generative art exciting. We also talked about whether generative-art is interpredable, because many people think that art needs to have a clear message or purpose to be significant. But i think many artists don't always have a specific meaning to their work, but leaves it to the spectators themselves to interpret it. And since i'm of the perception that the meaning of art is made mostly of the spectator themselves, i think computers and generative art can be the cause of many meaningful and surprising artworks. 

Lastly, it can be discussed whether the computer or I (the maker of the program) is responsible for the words that emerge. It could be that some words that appears would offend or insult a spectator of the program. Who should be blamed for this? Would it then be my fault, if I just told the program to generate letters, or can the computer and it's generative system be taking into account for this? This is what i wanted to reflect upon in relation to this weeks topic. 

Please also note that it wrote "AI" two times, i think the program has become self-aware... °o°

**References**

- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
- Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.
- Matrix Digital Rain in p5js. 2020: https://www.youtube.com/watch?v=13Ip2brYPto
- Matrix Digital Rain by "co-dart": https://github.com/co-dart/Matrix_Digital_Rain/blob/master/sketch.js

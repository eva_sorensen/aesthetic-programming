#### MINIX3

____

[Link to my MiniX3](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX3/)

[Click here](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX3/sketch.js) to see my repository. 

Here's a screenshot of one of the states of the program: 

![](Skærmbillede_2021-05-18_kl._15.10.59.png)


The design of my throbber is consisting of a circle. Because of a 'for-loop' it circulates in the middle of the canvas and returns. It's possible to change the design of the O into different signs. I just did this out of curiousity of making an array and making it work - but i didn't incorporate anything in the program that indicates the possibility of this change, since it's not really important for the overall message of the throbber. The colours of the throbber also changes when the mouse is clicked. 

There are several time-related syntaxes in my program: `frameRate(); and time += TWO_PI / 360;` are those i figured out influence the temporality in the program. The for-loop also influence the temporality in the way it chances the speed of the circulation when chancing parameters making the symbols circulate the center two times - or more - instead of just one. This syntax is in accordance to the littirature we've read in Aesthetic Programming (p. 91-94), which was very helpful in understanding what parameters i needed to chance to achieve something specific. 
To make the throbber i got inspired from the source code of Asterisk Painting and Tanabata, by Yurika Sayo. 

In my program, the background is a little bit transparent, and behind there's a picture with binary code to demonstrate what goes on "behind the stage" when the throbber is shown. With this i want to show that there can be many reasons for the throbber to show, as we've learned this week it's not allways because the page is just loading, but it can be shown just to build up an anticipation. 
In regards of the throbbers design, i've used different kinds of symbols to try an make the rotation of these more aesthetic. This i did because the use of throbbers is when the user is put on hold and is left there to wait. Therefore i wanted it to be nice to look at, so that the throbber can distract the user from the fact that they are waiting. 

All in all, i think throbbers in general express the time concept of digital culture in the way that they allways go in a loop. In some way i think the design of interfaces can influence many users and thinking about and experiencing time (ex. how quickly something is loading) in different ways. 

- Eva ヽ(^。^)丿

**References**

- Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

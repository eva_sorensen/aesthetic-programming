let words = ["O", "OK", "#", "⌘", "დ", "時"];
let index = 0;

let time = 0.0;
let fillColor;

let img;

function preload() {
  img = loadImage("binary_code.jpg");
}

function setup () {
 createCanvas(windowWidth, windowHeight);
 frameRate(80);
 image(img, 0, 0, windowWidth, windowHeight);

 fillColor = color(
   floor(random(0, 255)),floor(random(0, 255)),floor(random(0, 255))
 );
}

function draw () {
  //image(img, 0, 0, windowWidth, windowHeight);
  background(70, 50);
  push();
  drawElements();
  pop();

  //text in the left corner
  push();
  textSize(26);
  textStyle(ITALIC);
  noStroke();
  fill(106, 110, 175);
  text("just one more second of your time...", 50, windowHeight-50);
  pop();

}

function drawElements() {
  translate(width/2, height/2); //moves elements to the center
  for (let i=0; i<18; i++) { //for loop
  push();
  rotate(TWO_PI*i/18*(1-1*cos(time))); //specifics for the rotation
  noStroke();
  fill(fillColor);
  textSize(40);
  text(words[index], 40, 0); //text according to the index
  pop();
}
time += TWO_PI / 360; //time determines the rotation-speed among others

}

function mousePressed() {
//change the word and colour every time mouse is pressed
  index = index+1;

  fillColor = color(
    floor(random(0, 255)),floor(random(0, 255)),floor(random(0, 255))
  );

//make the index return to 0, when there are no more words in the array
  if(index == words.length) {
    index = 0;
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

## MiniX6 - Revisiting miniX2

[Link to the repository](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX6/sketch.js)

[Link to the PROGRAM](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX6/)

____

Here are some screenshots of my program.. They show the faces in their two different stages. 

![](Skærmbillede_2021-03-22_kl._14.03.51.png)

![](Skærmbillede_2021-03-22_kl._14.04.01.png)

**Which MiniX do you plan to rework?**

- I plan to revisit miniX2, which we've made in relations to the subject of variable geometry. I'm choosing this miniX because it lacks technical savvy and i think the conceptual aspect can demonstrate how critical making is a practice is aesthetic programming. 

**What have you changed and why?**

- I've made the text appear according to the size of the canvas. Before some of the text wasn't showing if the browser window opened too small. I also added a ellipse around the smileys. I thought it added a more relatable look of emoji. 
The things i've changed was for improving the functional aspect of the program, and then increasing the associations made to emojis. 

**How would you demonstrate aesthetic programming in your work?**

- The way i've demonstrated ap in the program, is mostly by the text and the change the faces make - going from happy or sad to the opposite with just few extra shapes. Also the text should emphasize this, by adressing it diectly. 
Hopefully, this will make the spectator wonder upon the subject of emojis and what they express, and in that way the program will be cause of reflection. 

**What does it mean by programming as a practice, or even as a method for design?**

- I think mostly the actual action of writing code and thinking about the conceptual together makes the practice of aesthetic programming. It can be a method for design in the way programming can be a way to express whatever the coder wishes to. 

**What is the relation between programming and digital culture?**

- Programming and digital culture is very much intertwined, since programming shapes everything digital. That is also why aetshetic programming as a practice is really important, since programming is shaping the digital world and in to some extend determining how we act online. 

**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**

- I think emojis as a general is a good example of how technology can impact our interactions online. Important for critical making is the making itself, and this is also a place for critical reflection, the way I have reflected upon emojis impact while making this program. 
Critical making is described to have following goal: "_Their aim is to make consumers more aware of the values, ideologies, and behavioral norms inscribed in the designs that are used in their everyday lives_" (p. 21). And this relates to my program since what i hope it does, it making spectators think about what impact emojis have on them and think critical upon this. 

**References**

- Bogers, Loes & Letizia Chiappini. "Critical Making and Interdisciplinary Learning: Making a Bridge between Art, Science, Engineering and Social Interventions", _in The Critical Makers Reader: (Un)learning Technology_. 2019. pp. 17-28
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24

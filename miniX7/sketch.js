let min_beer = 4;
let beer = [];
let fadøl;
let eva;
let evaPosX;
let evaPosY;
let evaSize = {
  w:135,
  h:120
};
let score = 0, lose = 0;

function preload() {
  fadøl = loadImage('beer-kopi.png');
  eva = loadImage('eva.JPG');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  evaPosX = windowWidth/2;
  evaPosY = windowHeight-140;
  //frameRate(40);
}

function draw() {
  background(180);

  checkBeerNum();
  showBeer();
  checkDrinking();

  image(eva, evaPosX, evaPosY, evaSize.w, evaSize.h);

  displayScore();
  checkResult();
}

function checkBeerNum() {
  if (beer.length < min_beer) {
    beer.push(new Beer());
    //print("We just got a beer :)",beer[0])
  }
}

function showBeer() {
  for (let i=0; i < beer.length; i++) {
    beer[i].move();
    beer[i].show();
  }
}

function checkDrinking() {
  for(let i=0; i < beer.length; i++){
    let d = int( // int = heltal
      dist(evaPosX+evaSize.w/2, evaPosY,
      beer[i].pos.x, beer[i].pos.y) // calculate dist
    );
    //print("Calculated a distance of",d)
    if (d < evaSize.w / 2 && beer[i].pos.y > evaPosY) { // close enough to "drink"
      score++;
      beer.splice(i, 1);
      //print("Caught beer with distance ", d);
    }
    else if (beer[i].pos.y >= windowHeight-3) { // misses
      lose++;
      beer.splice(i, 1);
    }
  }
}

function displayScore() {
  fill(50);
  textSize(20);
  text('You have drunk '+ score +" beer(s)", 30, windowHeight-50);
  text('You have dropped '+ lose +" beer(s)", 30, windowHeight-30);
  text('MOVE THE ARROWS TO DRINK THE BEER BERFORE DROPPING IT', 30, 30);
}

function checkResult() {
  if (lose > score && lose > 3) {
    fill(255, 51, 153);
    textSize(120);
    textAlign(CENTER);
    text('GAME OVER', windowWidth/2, windowHeight/2);
    textSize(40);
    text('Sadly, you have dropped too many beers', windowWidth/2, windowHeight/2+80);
    noLoop(); // stop game
  }
}

function keyPressed() {
  if (keyCode === 39) { // move right
    evaPosX += 50;
  } else if (keyCode === 37) { // move left
    evaPosX -= 50;
  }
// reset if the picture is moved out of the canvas
  if (evaPosX > windowWidth+5) {
    evaPosX = windowWidth/2;
  } else if (evaPosX < -5) {
    evaPosX = windowWidth/2;
  }
}

class Beer {
  constructor() { // properties of the beers
    this.speed = floor(random(1, 3));
    this.size = floor(random(20, 35));
    this.pos = new createVector(int(random(windowWidth)), 0);
    //print("Created beer at pos",this.pos)
  }
  move() {
    this.pos.y += this.speed; // behavior of the beers
  }
  show() {
    push();
    imageMode(CENTER);
    image(fadøl, this.pos.x, this.pos.y, this.size, this.size);
    pop();
  }
}

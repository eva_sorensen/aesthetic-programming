## MiniX7 - Game with objects

[Click here for repository](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX7/sketch.js)

[Click here to try the game](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX7/)

____

Screenshots of my program:

![](Skærmbillede_2021-03-29_kl._10.28.55.png)

![]( Skærmbillede_2021-03-29_kl._10.32.59.png)

**Introduction**

I've made a game where you need to catch the beers, before they miss. The game is called "Fredagsbar", because the bars are supposed to open on 5. may (hopefully). The game is strongly influenced by the Tofu-game by Winnie Soon ;)) 

- How does/do your game/game objects work?

There is a class of Beers, this determines how they fall down from the top of the canvas, and makes them appear according to an array that says that there can be 5 beers showing at a time. The class in general determines their properties and behavior. 

The picture, that is the games avatar you could call it, moves around using if-statements. Some for-loops and if-statements checks the score and result and also decides whether the beers has been drunk or if it was missed; and according to this adds point to the score or lose. 

-  How you program the objects and their related attributes, and the methods in your game.

I've created, much like the Tofu-game in class, different functions that all have different purposes. They are;
`checkBeerNum`
`showBeer`
`checkDrinking`
`displayScore`
`checkResult`
`class Beer`

The "objects" in the game are just loaded images that work with same methods as other objects in p5.js. The score is checked by calculating the distance between the beers and the image. If the beers are within reach, the program will see it as drunken and add 1+ to score. If the beers reaches a specific height (windowHeight-3) the program will consider this as missed and add 1+ to lose. Another statement determines whether there is too many beers dropped, and the game will be lost; `if (lose > score && lose > 3)`

- Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction?

As adressed in "The Obscure Objects of Object Orientation" by Matthew Fuller and Andrew Goffey, object orientation is an approach in computation, which seaks to view objects and their interactions on a programmable plane.  

> On this count, a computer program written in an object-oriented programming language develops a kind of intensional logic, in the sense that each of the objects that the program comprises has an internal conceptual structure that determines its relationship to what it ‘refers’ to, suggesting that objects are, in fact concepts, concepts that represent objects in a machinic materialization of a logical calculus. (p. 5)

Here the text proclaims, that objects according to OO in some ways are concepts. The same way this game, and all anothers, run on simple concepts that the user quite quickly interceps. Here the concept is to catch the beers, that are falling down. And even if the concept wasn't written in the top corner, i think the user would figure out the concept of the game anyway. 
Ofcourse, concepts can be understood in many different ways as OO is used in many instances. The way a concept of a program is determined is by classes and other properties of objects. 

- Your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

According to your textbook, Aesthetic Programming, objects and OO can be understood as following
> In other words, objects in OOP are not only about negotiating with the real world as a form of realism and representation, nor about the functions and logic that compose the objects, but the wider relations and “interactions between and with the computational.” (p. 145)

This weeks miniX is a concrete example of how OO also is about relations and interactions between the computational objects - ex. how the classes (beers) interacts when hitting the image - and how the user also interacts with the objects. Furthermore, abstraction can be understood on many levels of programming and computation. It is important to understand that a computer runs on several levels of abstraction, and the fact that this also conceals some of the details and processes of a computer. The relation between human (programmer) and the different levels of abstraction is never a neutral process, and there are many 'pitfalls' to avoid. But object abstraction helps
 > "to think conceptually about how computational objects model the world, and what this suggests in terms of an understanding of hidden layers of operation and meaning." (p. 146)

____

Happy Easter! :)

**References**

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017)

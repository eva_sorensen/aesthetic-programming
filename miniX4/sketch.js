let dancingWords = [];
let button;
//let mySound;
let x = 10;

/*function preload() {
  soundFormats('mp3', 'ogg');
  mySound = loadSound('gong');
}*/

//declaration of the class
class DanceSpan {
  constructor(element, x, y) {
    element.position(x, y);
    this.element = element;
    this.x = x;
    this.y = y;
  }

  //erratic movement
  brownian() {
    this.x += random(-x, x);
    this.y += random(-x, x);
    this.element.position(this.x, this.y);
  }
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(40);
  background(100);

  // create text (in a for-loop to create x times)
  for (let i = 0; i < 3; i++) {
    createP(
    ' the ways in which all aspects of our life seems to be turned into data ' +
      ' the patterns of human behavior is extracted ' +
      ' large quantities of all manner of data is harvested ' +
      ' and that button was seductive to you, ' +
      ' so you just had to push it '
  ).addClass('text').hide();
  }

  // grab the text
  const texts = selectAll('.text');

  /* for-loop extracting the words, seperating them
  and making them appear randomly on the canvas*/
  for (let i = 0; i < texts.length; i++) {
     const paragraph = texts[i].html(); // make paragraph of text
     const words = paragraph.split(' '); // seperate words
     for (let j = 0; j < words.length; j++) {
       const spannedWord = createSpan(words[j]);
       const dw = new DanceSpan(spannedWord, random(windowWidth), random(windowHeight));
       dancingWords.push(dw);
     }
   }

   // button and style
   button = createButton('do not push me');
   button.style('background-color', col = (color(255, 0, 0)));
   button.style('font-size', '10')
   button.position(windowWidth-100, windowHeight-60);
   button.size(80, 40);
   button.mousePressed(drawElements);
 }

 function draw() {
   for (let i = 0; i < dancingWords.length; i++) {
     dancingWords[i].brownian(); // makes words dance
   }
 }

 function drawElements() {
   button.remove(); // removes the button
   removeElements(); //removes the dancing words

   //mySound.play();
   background(100);

   // same text
   let s = 'the ways in which all aspects of our life seems to be turned into data\n the patterns of human behavior is extracted\n  large quantities of all manner of data is harvested.\n   and that button was seductive to you,\n    so you just had to push it';
   fill(255, 255, 0);
   textSize(18);
   textStyle(ITALIC);
   text(s, 100, 100, 600, 400); //text wraps within text box
 }

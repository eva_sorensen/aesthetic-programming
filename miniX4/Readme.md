### MiniX4 - "Capture All"

[My program](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX4/)

[My repository](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX4/sketch.js)

Here are some screenshots of my program: 

![](skærmbillede1.png)
![](skærmbillede2.png)

____

#### ReadMe
**A title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.** 

I've chosen to call my program "Life of data"; just because the movement of the words makes me think of organism and bacteria, and their erratic movement. It shows a short texts words vibrate around on the canvas. In the right bottom is a red button that says: "do not push me". But I expect everyone to push it anyways, since it's the only thing showing on screen that you can interact with. The words now assembles and shows the short text, that before was 'dancing' around. The text says something about data, and I've copied must of it from "Aesthetic Programming" by Winnie Soon and Geoff Cox and also wrotes a couple of the lines myself. 

**Your program and what you have used and learnt.** 

As described, my program exists of erratic moving words and a red button which says "do not push me" ("me" as if the button is someone who can talk - also parallel to Software studies and posthumanism). When the button is pushed, the text appear. 

The first thing in my program is a 'class' (with which you can make ex. objects) where the element is declared and the movement is defined. I've then used several for-loops, a button and lastly, the function ``drawElements`` is activated when the button is pushed. 
	
For this program I've learned a lot because i had so many problems making it. Most of the code and the idea i had from the examples in p5js.org "Modifying the DOM". When i added the button, the words stayed there and it took me some time to figure out, i needed to use the ``removeElements();`` to make them disappear, when the mouse is clicked. 
	
I experimented a lot with mic-input, radio buttons and sliders, but could not make any of it work how i wanted it to - so i left it out and focused elsewhere. In my code there are something i've removed, as you can see yourself; it was a sound that played when the button was pushed. The problem was, that it did not work when i uploaded to gitlab, and therefore i decided just to go without it even though it worked when i did the atom-live-server in Atom. 

**How your program and thinking address the theme of “capture all.” – What are the cultural implications of data capture?** 

The idea behind the program is to show how much data is floating around different platforms and interfaces without we, the users, noticing (or understanding). It is only when you collect different aspects and put it together (the floating words show this) that the data (text) makes sense and can be taken in use; analyzed for predictive behavior and everything that Shoshana Zuboff calls "surveillance capitalism".  In a way a lot of this capture of data happens behind 'closed doors' in more than one way. Many users aren't aware of exactly how much data they are deprived of, and blindly accept the use of cookies and other tools collecting data. One reason for this, is that users are exposed to so many different accept/decline-choices on the internet (and the decline-buttons is conveniently almost invisible), so it happens without anyone really knowing it is happening. Of course, companies doing this is not parading knowledge about this around since they're making money on it. And since technology has evolved so fast(and still is), there is yet no real laws against it. 
With the design of the button, I also wanted to send a message. I think it can symbolize different things: the first thing i though of was clickbait, and how you can be catfished in a way. But mostly, I draw on the idea that buttons (and anything else interactive) is inviting users to click and explore - even though it says not to, it's in the human nature to get curious.   

**References**

- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- https://p5js.org/examples/dom-modifying-the-dom.html
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.

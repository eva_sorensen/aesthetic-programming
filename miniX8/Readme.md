## MiniX8 
#### by Sofie Juul Nisted and Eva Sørensen

_____

[Repository](https://gitlab.com/eva_sorensen/aesthetic-programming/-/blob/master/miniX8/sketch.js)

[Run the program here](https://eva_sorensen.gitlab.io/aesthetic-programming/miniX8/)

![](Skærmbillede_2021-04-11_kl._19.48.41.png)

![]( Skærmbillede_2021-04-11_kl._19.47.44.png)

**- Provide a title for your work and a short description (1000 characters or less).**

Our work is titled “Miss Information”, and is a comment on misinformation sold to women from within the “lifestyle business”. Last year, Gwyneth Palthrow, actress and founder of Goop, premiered a Netflix-miniseries that delved into her company’s very unique view on beauty, wellness and especially science. Palthrow has long been known to suggest alternative treatments and remedies on her website*, and adopting a scientific-sounding ethos, but not necessarily being backed by actual scientists**. However, this is not a new phenomenon. Since the dawn of time, people have exchanged tips for improving their lives and looks, and for just as long, some have tried to abuse this demand for gains. From smearing your face with blood to bathing in milk, there have been too many ludicrous beauty tricks to mention. That is why this miniX is focused on misinformation specifically targeting women, in regards to beauty, romance and housework. 


**- Describe how your program works, and what syntax you have used and learnt.**

Our program consists of a kind of frontpage with some explainable text and a button. When the button is pressed, a new text will appear which is the main thing of the program. The text is generated on the basis of a JSON-file, which we’ve made ourselves. The JSON-files consist of three arrays: (1) nouns, (2) placements and (3) results. In the sketch, the syntax `floor(random())` determines a random index number for each array, and later on, that random index number is used for calling that specific word in the specific array. A word from every array will be called into the following sentence: “Put - (1) - into your - (2) - for (3)”. 
                        

**- Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).**
According to the text Aesthetic Programming and Vocable Code, code is somewhat like speaking, also known as the speech-act theory. This also tells that code can be poetry just like speech, and both poetry and code consists of words and is ‘performed’. 
Just like poems can be critical upon a subject, our program and code can be seen also as something that addresses and shows some problems in our society. The problems we’d like to address is as told in the first section about misinformation, especially taking into account how technology has made it possible to spread information easier and quicker than ever before. 

**- How would you reflect on your work in terms of Vocable Code**

> “_Thus, once the human “spoke”, it was understood to signify a higher human reason and a world of signification, as opposed to a mere voice that might simply comprise a collection of nonlexical vocables. Yet it would seem that computer speech simulation is rther too rational, further complicating the distinctions._” 

Here, Cox and McLean are commenting on the notion that the human “speech” is higher is status than the animal “voice”, and so the computer’s language must be higher yet, since it follows even more rigorous rules, which is also the case here. There is no audio in the program, and the text is outputted after the user has pressed a button. The program does not output text in a rhythm that resembles the flow of words that is a necessity and a given when speaking as a human. Our program is based on a long tradition of sharing tips and tricks, but it does not reflect how a dialogical exchange of suggestions would carry out. It leads the mind to a fortune-teller at a carnival, spitting out nonsensical statements, and claiming absolute authority on a topic. There have always been people gaining from other people’s trust in their expertise - now those people are online, and so a parallel is drawn between the misinformation of then and the misinformation of now.                     
                
            
**References**     

- https://goop.com/
- https://www.theguardian.com/commentisfree/2019/feb/08/goops-deal-with-netflix-is-a-dangerous-win-for-pseudoscience 
- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186
- Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.
